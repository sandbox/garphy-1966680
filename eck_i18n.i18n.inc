<?php

/**
 * @file
 * Internationalization (i18n) hooks
 */

/**
 * Implements hook_i18n_object_info().
 */
function eck_i18n_i18n_object_info() {

  foreach (eck_entity_info() as $entity_type => $entity_info) {

    // TODO add 'string translation' support ?
    $object_info[$entity_type] = array(
      'title' => $entity_info['label'],
      'class' => 'EckI18nEntity',
      'entity' => $entity_type,
      'key' => $entity_info['entity keys']['id'],
      'placeholders' => array(
        '%' => $entity_info['entity keys']['id'],
      ),
      'edit path' => "admin/structure/entity-type/{$entity_type}/translate/%",
      'translate tab' => "admin/structure/entity-type/{$entity_type}/translate/%",
      'translation set' => TRUE,
    );

  }

  return $object_info;
}

/**
 * Implements hook_i18n_translation_set_info().
 */
function eck_i18n_i18n_translation_set_info() {
  foreach (eck_entity_info() as $entity_type => $entity_info) {

    $translation_set_info[$entity_type] = array(
      'title' => $entity_info['label'],
      'class' => 'EckI18nTranslationSet',
      'table' => "eck_{$entity_type}",
      'field' => 'tsid',
      'placeholder' => '%eck_i18n_translation_set',
      'list path' => "admin/structure/entity-type/{$entity_type}/sets",
      'edit path' => "admin/structure/entity-type/{$entity_type}/sets/edit/%eck_i18n_translation_set",
      'delete path' => "admin/structure/entity-type/{$entity_type}/sets/delete/%eck_i18n_translation_set",
      'page callback' => 'eck_i18n_translation_page',
    );

  }
  return $translation_set_info;
}
