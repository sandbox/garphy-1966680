ECK_i18n is multilingual ECK entities management.

This module implements i18n API for language-enabled ECK entities with a
"translation set" strategy (1 entity per language).

Features :
* ECK entities translation sets
* Existing translations reference (planned)

The code is heavily borrowed from i18n_taxonomy.

A patch for ECK is needed to enabled translated entity preparation.
See #1966716: Add hook to prepare new entities : hook_eck_entity_prepare().
