<?php

/**
 * @file
 * eck_i18n classes
 */

/**
 * i18n Object Wrapper implementation for ECK entities
 */
class EckI18nEntity extends i18n_object_wrapper {

  /**
   * Get translate or localize mode for object.
   */
  public function get_translate_mode() {
    return I18N_MODE_TRANSLATE;
  }

  /**
   * Menu access callback for mixed translation tab.
   */
  public function get_translate_access() {
    $object_info = i18n_object_info($this->get_type());
    if (!$object_info) {
      return FALSE;
    }
    $entity = entity_load_single($object_info['entity'], $this->get_object());
    return $entity && Bundle::loadByMachineName($entity->entityType() . '_' . $entity->type)->config['translation']
        && i18n_object_langcode($entity);

  }

}

/**
 * i18n translation set implementation for ECK entities
 */
class EckI18nTranslationSet extends i18n_translation_set {

}
