<?php

/**
 * @file
 * Implementation of 'tsid' behavior, for storing the translation set id
 */

$plugin = array(
  'label' => "Translation set",
  'entity_info' => 'eck_i18n_tsid_property_entity_info',
  'property_info' => 'eck_i18n_tsid_property_property_info',
  'default_widget' => 'eck_i18n_tsid_property_widget',
  'bundle_form' => 'eck_i18n_tsid_property_bundle_form',
  'unique' => TRUE,
);

/**
 * Implementation of 'entity_info' plugin callback.
 */
function eck_i18n_tsid_property_entity_info($property, $var) {
  $info = $var;
  // Nodes also put its language into entity keys, so we do the same.
  $info['entity keys']['tsid'] = $property;
  return $info;
}

/**
 * Implementation of 'property_info' plugin callback.
 */
function eck_i18n_tsid_property_property_info($property, $vars) {
  $vars['properties'][$property] += array(
    'label' => t('Translation set'),
    'description' => t('Entity translation set.'),
  );
  return $vars;
}

/**
 * Implementation of 'default_widget' plugin callback.
 */
function eck_i18n_tsid_property_widget($property, $vars) {
  $entity = $vars['entity'];
  $tsid = $entity->{$property};
  return array(
    '#type' => 'textfield',
    '#title' => $vars['properties'][$property]['label'],
    '#default_value' => $tsid,
    '#disabled' => TRUE,
  );
}


/**
 * Implementation of 'bundle_form' plugin callback.
 */
function eck_i18n_tsid_property_bundle_form($property, $vars) {
  if (module_exists('locale')) {
    $bundle = $vars['form']['bundle']['#value'];
    $vars['form']['config_translation'] = array(
      '#type' => 'radios',
      '#title' => t('Translation support'),
      '#default_value' => isset($bundle->config["translation"]) ? $bundle->config["translation"] : 0,
      '#options' => array(t('Disabled'), t('Enabled')),
    );
  }
  return $vars;
}
